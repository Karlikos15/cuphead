package screens;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

import elements.Barril;
import elements.Player;
import game.Demo;
import game.Parametros;


public class GameScreen extends BScreen{
	
Stage mainStage;
Array<Barril> barriles;
OrthographicCamera camara;
	   
private Player player;

	public GameScreen(Demo game) {
		
		super(game);
		mainStage=new Stage();
		
		camara=(OrthographicCamera) mainStage.getCamera();
		camara.setToOrtho(false, Parametros.getAnchoPantalla()*Parametros.zoom, Parametros.getAltoPantalla()*Parametros.zoom);
		
		
		
		player=new Player(50,100,mainStage);
	barriles= new Array<Barril>();
		for(int i=0; i<10; i++) {
			barriles.add(new Barril(i*12,30,mainStage));
		}
		
		
		
		
		
		
	}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		super.render(delta);
	     mainStage.act();
	    colide();
	    
	    centrarCamara();
	    
	    
	        mainStage.draw();
	    

	}
	
	public void colide() {
		player.tocoSuelo=false;
		for(Barril b:barriles) {
			
			
			if(b.getEnabled() && b.overlaps(player)) {
				player.preventOverlap(b);
				
				//b.preventOverlap(player);
				
			}
			
			
			if(player.pies.overlaps(b)) {
				player.tocoSuelo=true;
			}
			
		}
		
	}
	
	public void centrarCamara() {
		this.camara.position.x=player.getX();
		this.camara.position.y=player.getY();
		camara.update();
		
	}
	
	
}
