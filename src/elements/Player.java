package elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;

import game.Parametros;

public class Player extends Element{
private Animation<TextureRegion> drcha;
private Animation<TextureRegion> izqda;
public Element pies;
public boolean tocoSuelo;

private float walkingSpeed=150;


	public Player(float x, float y, Stage s) {
		super(x, y, s);
		this.maxSpeed=150;
		this.deceleration=1000;
		tocoSuelo=false;
		
		
		drcha=this.loadFullAnimation("Cuphead/Idle/spriteIdle.png", 1, 5, 0.2f, true);
		izqda=this.loadFullAnimation("Cuphead/Idle/spriteIdle.png", 1, 5, 0.2f, true);
		this.setPolygon(8);
		
		
		pies=new Element(0, 0, s,this.getWidth() ,this.getHeight()/10 );
		pies.setRectangle();
	}
	
	


	@Override
	public void act(float delta) {
		
		super.act(delta);
		
		controles();
		
		//aplico graviedad
		this.acceleration.add(0,Parametros.gravedad);
		this.applyPhysics(delta);
		colocarPies();
		
	}
	
	private void controles() {
		
	if(Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.RIGHT)) {
		this.setAnimation(izqda);
		this.acceleration.add(-walkingSpeed,0);
	}
	
	if(Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.LEFT)) {
		this.setAnimation(drcha);
		this.acceleration.add(walkingSpeed,0);
	}
	if(Gdx.input.isKeyJustPressed(Keys.SPACE) && tocoSuelo) {
		salta();
	}
	
		
	}
	
	
	
	private void colocarPies() {
		this.pies.setPosition(this.getX(), this.getY()-(this.getScaleY()/2));
		
		
	}
	
	private void salta() {
		this.acceleration.add(0,20000);
		this.tocoSuelo=false;
		
		
	}

}
